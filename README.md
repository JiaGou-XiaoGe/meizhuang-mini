# meizhuang-mini qq 1439226817  qq群 617430878

#### 介绍
美妆小程序 是一款化妆品公司定制的一个小程序，采用原生的小程序开发的微信小程序。

#### 软件架构
软件架构说明

后台主要是springboot2 mybatis-plus mysql redis 前端 vue 微信小程序




#### 使用说明

后台管理体验 http://yjlive.cn/h5bak/

小程序演示 申请后 打开调试模式

![输入图片说明](https://images.gitee.com/uploads/images/2019/1129/120107_ae902a04_1530803.png "屏幕截图.png")

(https://images.gitee.com/uploads/images/2019/1129/120008_8ff27b53_1530803.png "屏幕截图.png")


#### 功能列表

 **后台管理** 

- 登录 ，首页数据概览
- 商品列表 商品分类  商品分组
- 用户列表
- 订单列表（不同状态）
- 运营 定制服务（刻字服务，定制卡片，特殊封套，包装盒） 卡片寄语（）
- 营销 基本营销（满额立减，限时折扣，套餐商品 规则商品 朋友代付）  赠品管理（首赠礼 满赠礼 选赠礼 单品礼赠 验证码赠礼）
-   优惠券（新人发卷 满额发券 购物发券 手工发券 发放记录）  抽奖有礼
- 应用 （分享助力 好友赠礼 定制礼盒）
- 店铺 （页面配置管理 页面模版 小程序设置 商品 二维码 启动广告 模版库消息 模版消息）
- 数据 （流量分析 交易分析 客群分析 商品分析）
- 设置  （商城设置 交易设置 支付设置 短信设置）
- 系统（用户管理 角色管理 店铺管理 操作日志）


 **小程序管理** 
基本商品的购物和营销使用

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
